# DhiSMS

## Installation

composer require "tallminds/dhisms"
composer update
Add Service provider to providers array
Register Alias 'DhiSms' in the alias array


## Configuration

To publish configuration, run php artisan vendor:publish

## How to Use

$sms = DhiSms::send('7777777', 'Hey there!');

$delivery = DhiSms::delivery('msg_id', 'msg_key');


