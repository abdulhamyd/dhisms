<?php
/**
 * Created by PhpStorm.
 * User: deemah
 * Date: 6/1/15
 * Time: 8:42 PM
 */

namespace Tallminds\DhiSms;


class DhiSms extends Facade {

    protected static function getFacadeAccessor() { return 'dhisms'; }

}