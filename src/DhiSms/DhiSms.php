<?php
/**
 * Created by PhpStorm.
 * User: Abdul Hameed Ahmed
 * Date: 6/1/15
 * Time: 7:57 PM
 */

namespace Tallminds\DhiSms;


class DhiSms {

    public function __construct()
    {

    }

    public function send($mobile, $message)
    {
        $data = [
            'username' => config('dhisms.username'),
            'password' => config('dhisms.password'),
            'number' => $mobile,
            'msg' => $message
        ];

        $xml = view('dhisms::send', compact('data'))->render();

        $response = $this->sendRequest($xml);

        $xmlresp = simplexml_load_string($response);
        $arr = json_decode(json_encode($xmlresp), 1);

        $msg_id = $arr['TELEMESSAGE_CONTENT']['RESPONSE']['MESSAGE_ID'];
        $msg_key = $arr['TELEMESSAGE_CONTENT']['RESPONSE']['MESSAGE_KEY'];
        $msg_status = $arr['TELEMESSAGE_CONTENT']['RESPONSE']['RESPONSE_STATUS'];
        $msg_status_des = $arr['TELEMESSAGE_CONTENT']['RESPONSE']['RESPONSE_STATUS_DESC'];

        $ret = [
            'message_id'        => $msg_id,
            'message_key'       => $msg_key,
            'message_status'    => $msg_status,
            'status_desc'       => $msg_status_des,
            'message'           => $message,
            'number'            => $mobile,
            'message_sent_at'   => Carbon::now()
        ];

        return $ret;

    }

    public function delivery($msg_id, $msg_key)
    {
        $data = [
            'msgid' => $msg_id,
            'msgkey' => $msg_key
        ];

        $xml = view('dhisms::delivery', compact('data'))->render();
        $response = $this->sendRequest($xml);

        $xmlresp = simplexml_load_string($response);

        $arr = json_decode(json_encode($xmlresp), 1);

        $status_id = $arr['TELEMESSAGE_CONTENT']['MESSAGE_STATUS']['STATUS_ID'];
        $status = $arr['TELEMESSAGE_CONTENT']['MESSAGE_STATUS']['RECIPIENT_STATUS']['DEVICE']['STATUS'];
        $delivered_date = $arr['TELEMESSAGE_CONTENT']['MESSAGE_STATUS']['RECIPIENT_STATUS']['DEVICE']['STATUS_DATE'];
        $number = $arr['TELEMESSAGE_CONTENT']['MESSAGE_STATUS']['RECIPIENT_STATUS']['DEVICE']['VALUE'];
        $delivery_status_desc = $arr['TELEMESSAGE_CONTENT']['MESSAGE_STATUS']['RECIPIENT_STATUS']['DEVICE']['DESCRIPTION'];

        $ret = [
            'status_id' => $status_id,
            'status' => $status,
            'status_desc' => $delivery_status_desc,
            'number' => $number,
            'delivered_at' => $delivered_date
        ];

        return $ret;
    }

    private function sendRequest($xml)
    {
        $url = config('dhisms::url');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type:application/xml"]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);

        $resp = curl_exec($curl);
        curl_close($curl);

        return $resp;
    }

}